﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DouchetteProg.Save;

namespace DouchetteProg.Verif
{
    public class Verification
    {
        /*This function is intended to separate a string passed as a parameter into packets.
         *Cutting occurs when the '-' character is encountered.It also call the bar code validation function 
         *If the barcode is valid, the function returns "true" otherwise it returns "false"
         
         *Input -> [barCode]
         *Output -> [True or False]*/
        public static bool SplitString(string barCode, Database database)
        {
            int start = 0, dashIndex = 0, packageNumber = 0;
            bool end = false;
            string[] packageOfBarCode = new string[3];
            while (end == false)
            {
                dashIndex = barCode.IndexOf('-', start);
                try
                {
                    packageOfBarCode[packageNumber] = barCode.Substring(start, dashIndex - start);
                    start = dashIndex + 1;
                    packageNumber++;
                }
                catch (ArgumentOutOfRangeException)
                {
                    try
                    {
                        packageOfBarCode[packageNumber] = barCode.Substring(start);
                        packageNumber++;
                    }
                    catch (System.IndexOutOfRangeException)
                    {
                        return false;
                    }
                    end = true;
                }
                catch (System.IndexOutOfRangeException)
                {
                    return false;
                }
            }
            if (VerificationNumberPackage(packageNumber) == false) return false;
            if (VerificationFirstPackage(packageOfBarCode[0]) == false) return false;
            if (VerificationOtherPackage(packageOfBarCode[1], packageOfBarCode[2]) == false) return false;

            database.InsertLineBDD(packageOfBarCode[0], packageOfBarCode[1], packageOfBarCode[2]);
            return true;
        }

        /*This function tests the number of packets present in the barcode,
         *If the number of package is 3, the function returns "true" otherwise it returns "false"
         
         * Input -> [int number of Package]
         * Output -> [True or False]*/
        public static bool VerificationNumberPackage(int numberPackage)
        {
            if (numberPackage != 3) return false;
            else return true;
        }

        /*This function checks that the first packet is composed of 3 letters,
         *it takes as input the first packet passed as a parameter and returns the value "True" if the packet is correct,
         *or "False" if the packet is not compliant
         * 
         * Input -> [string firstPackage]
         * Output -> [True or False]*/
        public static bool VerificationFirstPackage(string firstPackage)
        {
            int numberLetter = firstPackage.Length;
            foreach (char c in firstPackage)
            {
                if (!(char.IsLetter(c)) || numberLetter != 3) return false;
            }
            return true;
        }

        /*This function checks that the second and the third packet is composed of 4 numbers,
         *it takes as input the second and third packet passed as a parameter and returns the value "True" if the packets are correct,
         *or "False" if the packets are not compliant
         * 
         * Input -> [string secondPackage, string thirdPackage]
         * Output -> [True or False]*/
        public static bool VerificationOtherPackage(string secondPackage, string thirdPackage)
        {
            int lengthSecondPackage = secondPackage.Length;
            int lengthThirdPackage = thirdPackage.Length;
            bool isCorrect = true;
            foreach (char number in secondPackage)
            {
                if (!(char.IsNumber(number)) || lengthSecondPackage != 4) isCorrect = false;
            }
            foreach (char number in thirdPackage)
            {
                if (!(char.IsNumber(number)) || lengthThirdPackage != 4) isCorrect = false;
            }
            if (isCorrect == true && lengthThirdPackage ==4 && lengthSecondPackage == 4) return true;
            else return false;
        }
    }

}
