﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.IO;

namespace DouchetteProg.Save
{
    public class Database
    {
        public SQLiteConnection sqlc;

        /*
         * This constructor is used to know if the sqlite file exists or not,
         * but it also gives the link for this connection
         */
        public Database()
        {
            sqlc = new SQLiteConnection("Data Source=douchette.sqlite;UTF8Encoding=True;");
            if (!File.Exists("./douchette.sqlite"))
            {
                SQLiteConnection.CreateFile("douchette.sqlite");
            }
        }

        /*
         * This function is used to connect to the database.
         * 
         * input -> []
         * output -> []
         */
        public void ConnectBDD()
        {
            if(sqlc.State != System.Data.ConnectionState.Open)
            {
                sqlc.Open();
            }
        }

        /*
         * This function is used to create table in the database.
         * 
         * input -> []
         * output -> []
         */
        public void CreateTable()
        {
            string createtable = "CREATE TABLE Produit (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,Type TEXT NOT NULL, Year_Production TEXT NOT NULL, Reference TEXT NOT NULL)";
            SQLiteCommand cmd = new SQLiteCommand(createtable, sqlc);
            cmd.ExecuteNonQuery();
        }

        /*
         * This function is used to check if the table is created.
         * 
         * input -> []
         * output -> [False or True]
         */
        public bool CheckTable()
        {
            try
            {
                SQLiteCommand cmd = new SQLiteCommand("SELECT Reference FROM Produit", sqlc);
                cmd.ExecuteNonQuery();
            }
            catch
            {
                return false;
            }
            return true;
        }

        /*
         * This function is used to insert data into the table.
         * 
         * input -> [string type, year and reference]
         * output -> []
         */
        public void InsertLineBDD(string type, string year, string reference)
        {
            ConnectBDD();

            if (!CheckTable() == true)
            {
                CreateTable();
            }

            String insert = "INSERT INTO Produit(Type,Year_Production,Reference) VALUES('" + type + "' , '" + year + "' , '"+ reference+ "')";
            SQLiteCommand cmd = new SQLiteCommand(insert, sqlc);
            cmd.ExecuteNonQuery();

            DisconnectBDD();

        }
        /*
         * This function is used to disconnect to the database.
         * 
         * input -> []
         * output -> []
         */
        public void DisconnectBDD()
        {
            if (sqlc.State != System.Data.ConnectionState.Closed)
            {
                sqlc.Close();
            }
        }

    }
}
